package com.co4gsl;

import com.co4gsl.AddressLookupApplication;
import com.co4gsl.services.PostcodeLookupService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.*;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;


/**
 * Created by rpayal on 22/02/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AddressLookupApplication.class)
@WebAppConfiguration
public class AddressLookupIntegrationTest {
    @Autowired
    private WebApplicationContext webApplicationContext;
    @Autowired
    private PostcodeLookupService postcodeLookupService;

    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    private final String FULL_ADDRESS = "Full UK address";

    @Before
    public void setup() throws Exception {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void testLookup_forPostcode_W60LG() throws Exception {
        String postcode = "W6 0LG";
        this.mockMvc.perform(post("/address")
                .contentType(MediaType.APPLICATION_JSON)
                .param("postcode", postcode))
                .andExpect(status().isOk())
                .andExpect(view().name("Address"))
                .andExpect(model().attribute("postcode", equalTo(postcode)))
                .andExpect(model().attribute("address", equalTo("Hammersmith Grove, London W6 0LG, UK")));
    }

    @Test
    public void testLookup_forPostcode_BT486DQ() throws Exception {
        String postcode = "BT48 6DQ";
        this.mockMvc.perform(post("/address")
                .contentType(MediaType.APPLICATION_JSON)
                .param("postcode", postcode))
                .andExpect(status().isOk())
                .andExpect(view().name("Address"))
                .andExpect(model().attribute("postcode", equalTo(postcode)))
                .andExpect(model().attribute("address", equalTo("Londonderry BT48 6DQ, UK")));
    }

    @Test
    public void testLookup_forPostcode_SW1A2AA() throws Exception {
        String postcode = "SW1A 2AA";
        this.mockMvc.perform(post("/address")
                .contentType(MediaType.APPLICATION_JSON)
                .param("postcode", postcode))
                .andExpect(status().isOk())
                .andExpect(view().name("Address"))
                .andExpect(model().attribute("postcode", equalTo(postcode)))
                .andExpect(model().attribute("address", equalTo("London SW1A 2AA, UK")));
    }
}