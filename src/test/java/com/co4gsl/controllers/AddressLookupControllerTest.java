package com.co4gsl.controllers;

import com.co4gsl.AddressLookupApplication;
import com.co4gsl.domain.Address;
import com.co4gsl.domain.LookupResult;
import com.co4gsl.domain.vo.AddressVO;
import com.co4gsl.services.PostcodeLookupService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.web.context.WebApplicationContext;

import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

/**
 * Created by rpayal on 22/02/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AddressLookupApplication.class)
@WebAppConfiguration
public class AddressLookupControllerTest {
    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockMvc mockMvc;

    @MockBean
    private PostcodeLookupService postcodeLookupService;

    @Autowired
    private ObjectMapper objectMapper;

    private final String FULL_ADDRESS = "Full UK address";
    private final String POSTCODE = "W6 0LG";

    @Before
    public void setup() throws Exception {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void testInitialForm() throws Exception {
        mockMvc.perform(get("/").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(view().name("FindAddress"));
    }

    @Test
    public void testAddressLookup() throws Exception {
        given(this.postcodeLookupService.getAddress(POSTCODE))
                .willReturn(getLookupResult());
        this.mockMvc.perform(post("/address")
                .contentType(MediaType.APPLICATION_JSON)
                .param("postcode", POSTCODE))
//                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(view().name("Address"))
                .andExpect(model().attribute("postcode", equalTo(POSTCODE)))
                .andExpect(model().attribute("address", equalTo(FULL_ADDRESS)));
    }

    @Test
    public void testAddressLookup_forNullPostcode() throws Exception {
        this.mockMvc.perform(post("/address")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new AddressVO())))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(view().name("FindAddress"));
    }

    private AddressVO getAddressVO() {
        AddressVO addressVO = new AddressVO();
        addressVO.setPostcode(POSTCODE);
        return addressVO;
    }

    private LookupResult getLookupResult() {
        LookupResult lookupResult = new LookupResult();
        lookupResult.setAddresses(asList(getAddress()));
        return lookupResult;
    }

    private Address getAddress() {
        Address address = new Address();
        address.setFormattedAddress(FULL_ADDRESS);
        return address;
    }
}