package com.co4gsl.controllers;

import com.co4gsl.domain.LookupResult;
import com.co4gsl.domain.vo.AddressVO;
import com.co4gsl.services.PostcodeLookupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Created by rpayal on 21/02/2017.
 */
@Controller
@RequestMapping("/")
public class AddressLookupController {
    private PostcodeLookupService postcodeLookupService;

    @Autowired
    public AddressLookupController(PostcodeLookupService postcodeLookupService) {
        this.postcodeLookupService = postcodeLookupService;
    }

    @GetMapping("/")
    String home(AddressVO addressVO) {
        return "FindAddress";
    }

    @PostMapping("/address")
    String findAddress(@Valid @ModelAttribute AddressVO addressVO, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            return "FindAddress";
        }
        LookupResult lookupResult = postcodeLookupService.getAddress(addressVO.getPostcode());
        //picking one address
        model.addAttribute("postcode", addressVO.getPostcode());
        model.addAttribute("address", getAddress(lookupResult));
        return "Address";
    }

    private String getAddress(LookupResult lookupResult) {
        return lookupResult.getAddresses() != null ? lookupResult.getAddresses().get(0).getFormattedAddress() : "";
    }
}