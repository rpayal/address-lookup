package com.co4gsl.domain.vo;

import javax.validation.constraints.NotNull;

/**
 * Created by rpayal on 21/02/2017.
 */
public class AddressVO {
    @NotNull
    private String postcode;

    public void setPostcode(String postcode) {
        this.postcode = postcode==""?null:postcode;
    }

    public String getPostcode() {
        return postcode;
    }
}
