package com.co4gsl.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class LookupResult {

    @JsonProperty("results")
    private List<Address> addresses = null;
    @JsonProperty("status")
    private String status;

    @JsonProperty("results")
    public List<Address> getAddresses() {
        return addresses;
    }

    @JsonProperty("results")
    public void setAddresses(List<Address> addresses) {
        this.addresses = addresses;
    }

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }
}