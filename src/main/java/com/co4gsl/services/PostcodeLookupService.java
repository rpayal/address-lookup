package com.co4gsl.services;

import com.co4gsl.domain.LookupResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * Created by rpayal on 21/02/2017.
 */
@Service
public class PostcodeLookupService {
    private static final Logger log = LoggerFactory.getLogger(PostcodeLookupService.class);

    @Autowired
    private RestTemplate restTemplate;

    @Value("${postcode.lookup.url}")
    private String postcodeLookupUrl;

    public LookupResult getAddress(String postcode) {
        log.info("Address lookup - calling Google Maps REST Web Service for postcode ({})", postcode);
        return restTemplate.getForObject(postcodeLookupUrl + postcode, LookupResult.class);
    }
}
