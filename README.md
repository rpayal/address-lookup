# README #

address-lookup service 

### What is this repository for? ###
This is to get address for a postcode.

It further calls googleapi to make a address lookup search for a postcode.

### How do I get set up? ###
To run it from IDE
>>
gradle clean build
java -jar build/libs/*.jar

>> 
gradle bootrun

### Invoking application endpoints ###
http://localhost:8090
Will display a page to accept postcode (for which to fetch address)

### Who do I talk to? ###

* Repo owner or admin (rpayal)
